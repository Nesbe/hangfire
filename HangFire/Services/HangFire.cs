﻿using Hangfire;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HangFire.Services
{
    public class HangFire
    {
        public static void Configuration(IApplicationBuilder app)
        {
            app.UseHangfireServer();
            app.UseHangfireDashboard("/hangfire");

            //var options = new DashboardOptions
            //{
            //    Authorization = new[] { new MyAuthorizationFilter() },
            //    IsReadOnlyFunc = (DashboardContext context) => true
            //};
            //app.UseHangfireDashboard("/hangfire", options);
        }
        public static void InitializeJobs()
        {
            RecurringJob.AddOrUpdate<HangFireServices>(x => x.DoSomething(), Cron.Minutely);
        }
    }
}
