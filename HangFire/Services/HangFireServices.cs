﻿using HangFire.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HangFire.Services
{
    public class HangFireServices
    {
        protected ApplicationDbContext db;

        public HangFireServices(ApplicationDbContext context)
        {
            db = context;
        }

        public async Task DoSomething()
        { 
        }
    }
}
